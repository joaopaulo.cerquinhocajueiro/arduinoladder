/*
  libPlc Version 0.1, last updated 6th June, 2015.
  A simple IL language for Programmable Logic Controller (PLC) running
  on Arduino and compatibles.

  Author:    J.P.C. Cajueiro
  
  Based on:
  plcLib Version 1.0, last updated 23rd December, 2014.
  
  Author:    W. Ditch
  Publisher: www.electronics-micros.com

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details, available from:
  <http://www.gnu.org/licenses/>
*/


#include "Arduino.h"
#include "arduinoladder.h"


//////// Class IO ////////

IO::IO(int pin, unsigned int direction, unsigned int mode){
	_pin=pin;
	_dir=direction;
	_mode=mode;
	_value=0;
	pinMode(_pin,_dir);
}

IO::IO(int pin, unsigned int direction){
	_pin=pin;
	_dir=direction;
	_mode=0;
	_value=0;
	pinMode(_pin,_dir);
}

unsigned int IO::getValue(void){
	if(_dir==INPUT){ 				//if it is an input
		_lastValue = _value;
		if(_mode==ANALOG){ 				//if it is analog
			_value=analogRead(_pin);
		} else if (_mode==INVERTED){ 		//if it is inverted digital
			_value=!digitalRead(_pin);
		} else { 					//if it is digital
			_value=digitalRead(_pin);
		}
	}
	return _value;
}

unsigned int IO::read(void){
	return _value;
}

unsigned int IO::readLast(void){
	return _lastValue;
}

void IO::write(int value){
	if(_dir!=INPUT){ 				//if it is output
		_value = value; 				//update the internal value
	}
}

void IO::updateOutput(){
	if(_dir==OUTPUT){ 				//if it is output
		_lastValue = _value;
		if(_mode==ANALOG){ 				//if it is analog
			analogWrite(_pin,_value/4);
		} else if (_mode==INVERTED){ 		//if it is inverted digital
			digitalWrite(_pin,!_value);
		} else { 					//if it is digital
			digitalWrite(_pin,_value);
		}
	}
}


//////// Class Mem ////////

Mem::Mem(unsigned int initialValue){
	_value = initialValue;
}

Mem::Mem(void){
	_value=0;
}

unsigned int Mem::getValue(void){
	_lastValue = _value;
	return _value;
}

unsigned int Mem::read(void){
	return _value;
}

unsigned int Mem::readLast(void){
	return _lastValue;
}

void Mem::write(int value){
	_value = value; 				//update the internal value
}


//////// Class Node ////////

Node::Node(void){
  _value = 0;
  _lastValue = 0;
}

unsigned int Node::read(void){
	return _value;
}

unsigned int Node::readLast(void){
	return _lastValue;
}

void Node::write(int value){
	_lastValue = _value;
	_value = value; 				//update the internal value
}


//////// Class Counter ////////

Counter::Counter(unsigned long presetValue)	// Counter constructor method
{									// (Default values are for an up counter)
	setPoint = presetValue;		    				// Set preset value using supplied parameter
	value = 0;			    			// Running count = zero
}

Counter::Counter(unsigned long presetValue, unsigned long startValue)	// Counter constructor method
{									// (Default values are for an up counter)
	setPoint = presetValue;		    				// Set preset value using supplied parameter
	value = startValue;			    			// Running count = zero
}

void Counter::reset(Node& input)				// Clear counter method
{
	if(input.read()) {			// Enabled if scanValue = 1
		value = 0;  					// Running count = 0
	}
}

void Counter::preset(Node& input)				// Preset counter method
{
	if(input.read()) {			// Enabled if scanValue = 1
		value = setPoint;					// Running count = preset value
	}
}

void Counter::up(Node& input)				// Count up method
{
	if (input.read() && !input.readLast()) { // clock = 0 so clear counter edge detect
	Serial.println("Up");
		value++;				// Increment count
	}
}

void Counter::down(Node& input)				// Count up method
{
	if (input.read() && !input.readLast()) { // clock = 0 so clear counter edge detect
		value--;				// Decrement count
	}
}

unsigned int Counter::zero(Node& input){
	return input.read() && !value;
}

unsigned int Counter::done(Node& input){
	return input.read() && (value == setPoint);
}


//////// class Timer ////////

Timer::Timer(unsigned int type, unsigned long presetValue){
	_type = type;
	setPoint = presetValue;
	value = 0;
	_status = 0;
}

unsigned int Timer::update(Node& input){
	unsigned int output = 0;
	switch(_type){
	case 0: // TON
		if (!input.read()) {									// timer is disabled
			_status = 0;										// Clear timerState (0 = 'not started')
			value = 0;
		} else {													// Timer is enabled
			if (_status == 0) {								// Timer hasn't started counting yet
				_status = 1;	
				_start = millis();							// Set timerState to current time in milliseconds
				// output = 0;									// Result = 'not finished' (0)
			} else if (_status == 1){
				value = millis() - _start;												// Timer is active and counting
				if (value > setPoint) {	// Timer has finished
					output = 1;								// Result = 'finished' (1)
					_status = 2;
				}
			} else if (_status == 2){
				output = 1;
			}
		}
		break;
	case 1: // TOF
		output = 1;
		if (input.read()) {									// Timer input is on (scanValue = 1)
			_status = 0;
			value = 0;
		} else {													// Timer is enabled
			if (_status == 0) {								// Timer hasn't started counting yet
				_status = 1;	
				_start = millis();							// Set timerState to current time in milliseconds
				// output = 0;									// Result = 'not finished' (0)
			} else if (_status == 1){
				value = millis() - _start;												// Timer is active and counting
				if (value > setPoint) {	// Timer has finished
					output = 0;								// Result = 'finished' (1)
					_status = 2;
				}
			} else if (_status == 2){
				output = 0;
			}
		}
		break;
	case 2: // TP
		output = 0;
		if (input.read() && !input.readLast() && (_status==0)){
			_status = 1;
			_start = millis();
			output = 1;
		} else if (_status == 1){
			value = millis() - _start;
			if (value > setPoint){
				output = 0;
				_status = 0;
			} else {
				output = 1;
			}
		}
		break;
	default:
		break;
	}
	return output;
}


//////// Contacts ////////

unsigned int ContactNO(IO& value, Node& input){
	return input.read() && value.read();
}

unsigned int ContactNO(Mem& value, Node& input){
  return input.read() && value.read();
}

unsigned int ContactNC(IO& value, Node& input){
	return input.read() && !value.read();
}

unsigned int ContactNC(Mem& value, Node& input){
  return input.read() && !value.read();
}

unsigned int ContactRise(IO& value, Node& input){
	return input.read() && (value.read() && !value.readLast());
}

unsigned int ContactRise(Mem& value, Node& input){
  return input.read() && (value.read() && !value.readLast());
}

unsigned int ContactFall(IO& value, Node& input){
	return input.read() && (!value.read() && value.readLast());
}

unsigned int ContactFall(Mem& value, Node& input){
  return input.read() && (!value.read() && value.readLast());
}


///////// Timers ///////////

unsigned int ContactTON(Timer& timer, Node& input) {
	return timer.update(input);
}

unsigned int ContactTP(Timer& timer, Node& input) {
	return timer.update(input);
}


unsigned int ContactTOF(Timer& timer, Node& input) {
	return timer.update(input);
}


//////// Coils ////////

void CoilNO(IO& value, Node& input){ 
	value.write(input.read());
}

void CoilNO(Mem& value, Node& input){ 
  value.write(input.read());
}

void CoilNC(IO& value, Node& input){
	value.write(!input.read());
}

void CoilNC(Mem& value, Node& input){
  value.write(!input.read());
}

void CoilSet(IO& value, Node& input){
	if(input.read()){
		value.write(1);
	}
}

void CoilSet(Mem& value, Node& input){
  if(input.read()){
    value.write(1);
  }
}

void CoilReset(IO& value, Node& input){
	if(input.read()){
		value.write(0);
	}
}

void CoilReset(Mem& value, Node& input){
  if(input.read()){
    value.write(0);
  }
}


//////// Counters ////////

unsigned int Contact0(Counter& counter, Node& input){
	return counter.zero(input);
}

unsigned int ContactDone(Counter& counter, Node& input){
	return counter.done(input);
}

void CoilUp(Counter& counter, Node& input){
	counter.up(input);
}

void CoilDn(Counter& counter, Node& input){
	counter.down(input);
}

void CoilSet(Counter& counter, Node& input){
	counter.preset(input);
}

void CoilReset(Counter& counter, Node& input){
	counter.reset(input);
}
