// Code automatically generated from ladder

#include "arduinoladder.h"

// Pins
const int pina = 2;
const int pinb = 3;
const int pinc = 4;
const int pind = 5;
const int pinq = 8;
const int pincoiso = 9;
const int pinvalvula = 10;
// Nodes
Node node0;
Node node1;
Node node2;
Node node3;
Node node4;

IO a(pina, INPUT);
IO b(pinb, INPUT);
IO c(pinc, INPUT);
IO d(pind, INPUT);
Mem x;
Mem y;
Mem z;
IO q(pinq, OUTPUT);
IO coiso(pincoiso, OUTPUT);
IO valvula(pinvalvula, OUTPUT);
Counter c0(2);
Counter c1(2);
Counter c2(2);
Counter c3(2);

void setup(){

  node0.write(HIGH); // Node 0 is the phase - it is always HIGH

  q.write(LOW);
  coiso.write(LOW);
  valvula.write(LOW);
  c0 = 0;
  c1 = 0;
  c2 = 0;
  c3 = 0;
}

void loop(){
  //Read all inputs
  a.getValue();
  b.getValue();
  c.getValue();
  d.getValue();
  x.getValue();
  y.getValue();
  z.getValue();

  //Execute the code
  node1.write(ContactNO(a, node0));
  node2.write(ContactNC(b, node1));
  node3.write(ContactNO(a, node0) || ContactRise(b, node0));
  node4.write(ContactNO(c, node3));
  CoilSet(x, node2);
  CoilReset(x, node4);

  //Write all outputs
  q.updateOutput();
  coiso.updateOutput();
  valvula.updateOutput();
}

