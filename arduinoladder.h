/*
  libPlc Version 0.1, last updated 6th June, 2015.
  A simple IL language for Programmable Logic Controller (PLC) running
  on Arduino and compatibles.

  Author:    J.P.C. Cajueiro
  
  Based on:
  plcLib Version 1.0, last updated 23rd December, 2014.
  

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details, available from:
  <http://www.gnu.org/licenses/>
*/

#ifndef libLadder_h
#define libLadder_h

#include "Arduino.h"

#define DIRECT 0
#define INVERTED 1
#define ANALOG 2

class IO
{
public:
  IO(int pin, unsigned int direction);
  IO(int pin, unsigned int direction, unsigned int mode);
  unsigned int getValue(void);
  unsigned int read(void);
  unsigned int readLast(void);
  void write(int value);
  void updateOutput(void);
private:
  int _value;
  int _lastValue;
  int _pin;
  unsigned int _dir;
  unsigned int _mode;
};

class Mem
{
public:
  Mem(void);
  Mem(unsigned int initialValue);
  unsigned int getValue(void);
  unsigned int read(void);
  unsigned int readLast(void);
  void write(int value);
private:
  int _value;
  int _lastValue;
};

class Node
{
public:
  Node(void);
  unsigned int read(void);
  unsigned int readLast(void);
  void write(int value);
private:
  int _value;
  int _lastValue;
};

class Counter
{
public:
  Counter(unsigned long presetValue);
  Counter(unsigned long presetValue, unsigned long startValue);
  unsigned long value;
  unsigned long setPoint;
  void reset(Node& input);
  void preset(Node& input);
  void up(Node& input);
  void down(Node& input);
  unsigned int zero(Node& input);
  unsigned int done(Node& input);
};

class Timer
{
public:
  Timer(unsigned int type, unsigned long presetValue);
  unsigned long value;
  unsigned long setPoint;
  unsigned int update(Node& input);
private:
  unsigned int _type;
  unsigned int _status;
  unsigned long _start;
};

unsigned int ContactNO(IO& value, Node& input);
unsigned int ContactNC(IO& value, Node& input);
unsigned int ContactRise(IO& value, Node& input);
unsigned int ContactFall(IO& value, Node& input);

unsigned int ContactNO(Mem& value, Node& input);
unsigned int ContactNC(Mem& value, Node& input);
unsigned int ContactRise(Mem& value, Node& input);
unsigned int ContactFall(Mem& value, Node& input);

unsigned int ContactTON(Timer& timer, Node& input);
unsigned int ContactTOF(Timer& timer, Node& input);
unsigned int ContactTP(Timer& timer, Node& input);

void CoilNO(IO& value, Node& input);
void CoilNC(IO& value, Node& input);
void CoilSet(IO& value, Node& input);
void CoilReset(IO& value, Node& input);

void CoilNO(Mem& value, Node& input);
void CoilNC(Mem& value, Node& input);
void CoilSet(Mem& value, Node& input);
void CoilReset(Mem& value, Node& input);

unsigned int Contact0(Counter& counter, Node& input);
unsigned int ContactDone(Counter& counter, Node& input);

void CoilUp(Counter& counter, Node& input);
void CoilDn(Counter& counter, Node& input);
void CoilSet(Counter& counter, Node& input);
void CoilReset(Counter& counter, Node& input);

#endif
